NoTableTest
-----------

This test tests the behavior if no table was created within the test scope.

It expects a `MDLinterException("No table defined for test: 'NoTableTest'...")` Exception.
