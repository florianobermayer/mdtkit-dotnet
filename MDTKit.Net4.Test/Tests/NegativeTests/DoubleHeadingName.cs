#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class DoubleHeadingName : MDTestCase
    {
        [TestMethod]
        public void DoubleHeadingNameTest()
        {
            TestUtil.Assert.ExpectException(typeof (MDLinterException),
                "Duplicate header: '",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}