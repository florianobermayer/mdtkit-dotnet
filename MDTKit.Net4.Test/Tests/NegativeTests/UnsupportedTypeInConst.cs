#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class UnsupportedTypeInConst : MDTestCase
    {
        [TestMethod]
        public void UnsupportedTypeInConstTest()
        {
            TestUtil.Assert.ExpectExceptionInAggregateExceptionOrDirectly(typeof (MDLinterException),
                "Unsupported type: 'unsupported_type",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}