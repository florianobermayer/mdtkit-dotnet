#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class NoTable : MDTestCase
    {
        [TestMethod]
        public void NoTableTest()
        {
            TestUtil.Assert.ExpectException(typeof (MDLinterException), "No table defined for test: 'NoTableTest'",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}