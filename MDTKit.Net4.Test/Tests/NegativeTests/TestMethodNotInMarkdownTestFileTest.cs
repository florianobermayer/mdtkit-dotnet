﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    public class TestMethodNotInMarkdownTestFileTest : MDTestCase
    {
        [TestMethod]
        public void ThisTestMethodIsNotFoundInTheTestFile()
        {
            TestUtil.Assert.ExpectException(typeof(MDParserException), "Cannot find 'ThisTestMethodIsNotFoundInTheTestFile' in file 'TestMethodNotInMarkdownTestFileTest.md'!", 
            () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose));
        }
    }
}
