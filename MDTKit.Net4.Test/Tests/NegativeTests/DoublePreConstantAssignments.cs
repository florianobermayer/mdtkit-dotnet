#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class DoublePreConstantAssignments : MDTestCase
    {
        [TestMethod]
        public void DoublePreConstantAssignmentsTest()
        {
            TestUtil.Assert.ExpectException(typeof (MDLinterException),
                "Duplicate declaration of pre-constant 'double_assigned_var'",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}