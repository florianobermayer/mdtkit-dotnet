#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class NotProperlyFormattedStringInTable : MDTestCase
    {
        [TestMethod]
        public void NotProperlyFormattedStringInTableTest()
        {
            TestUtil.Assert.ExpectException(typeof (MDLinterException),
                "Could not resolve arithmetic expression: 'I am mal-formatted because I'm not surrounded by matching quotes':\nSyntaxError: Unexpected identifier",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}