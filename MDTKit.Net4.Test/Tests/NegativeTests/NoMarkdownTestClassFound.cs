#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class NoMarkdownTestClassFound : MDTestCase
    {
        [TestMethod]
        public void NoMarkdownTestClassFoundTest()
        {
            TestUtil.Assert.ExpectException(typeof (MDParserException),
                "No file found: 'MyNotInMarkdownTestFolderFindableClass.md'",
                () =>
                    RunMarkdownTestRowByRow("NoMarkdownTestClassFoundTest",
                        typeof (MyNotInMarkdownTestFolderFindableClass), MDTKitHelper.RunVerbose));
        }
    }

    internal class MyNotInMarkdownTestFolderFindableClass
    {
    }
   
}