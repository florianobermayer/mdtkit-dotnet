#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class MultipleMarkdownTestMethodsFound : MDTestCase
    {
        [TestMethod]
        public void MultipleMarkdownTestMethodsFoundTest()
        {
            TestUtil.Assert.ExpectExceptionInAggregateExceptionOrDirectly(typeof (MDLinterException),
                "Duplicate declaration of TestCase 'MultipleMarkdownTestMethodsFoundTest'",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}