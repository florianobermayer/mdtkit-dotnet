#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests.NegativeTests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class NoMarkdownTestMethodFound : MDTestCase
    {
        [TestMethod]
        public void NoMarkdownTestMethodFoundTest()
        {
            TestUtil.Assert.ExpectExceptionInAggregateExceptionOrDirectly(typeof (MDLinterException),
                "Expected \"|\", codeblock, newline or test case",
                () => RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose), compareMsgStartsWith: true);
        }
    }
}