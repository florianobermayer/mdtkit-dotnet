﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace MDTKit.Net4.Test.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class MDTKitTests : MDTestCase
    {
        [TestMethod]
        public void VerboseSmokeTest()
        {
            Dictionary<string, dynamic> preConst = null;
            Dictionary<string, dynamic> postConst = null;
            Dictionary<string, dynamic> preLinkedConst = null;
            Dictionary<string, dynamic> postLinkedConst = null;
            Dictionary<string, dynamic> preVar = null;
            Dictionary<string, dynamic> postVar = null;

            RunMarkdownTestRowByRow((row, index, pre, post) =>
            {
                MDTKitHelper.RunVerbose(row, index, pre, post);

                // const tests
                if (preConst == null || postConst == null || preVar == null || postVar == null || preLinkedConst == null ||
                    postLinkedConst == null)
                {
                    preConst = pre.Where(o => o.Key.StartsWith("c_"))
                        .ToDictionary(o => o.Key.Replace("c_", ""), o => o.Value);
                    postConst = post.Where(o => o.Key.StartsWith("c_"))
                        .ToDictionary(o => o.Key.Replace("c_", ""), o => o.Value);
                    preLinkedConst = pre.Where(o => o.Key.StartsWith("cl_"))
                        .ToDictionary(o => o.Key.Replace("cl_", ""), o => o.Value);
                    postLinkedConst =
                        post.Where(o => o.Key.StartsWith("cl_"))
                            .ToDictionary(o => o.Key.Replace("cl_", ""), o => o.Value);
                    preVar =
                        pre.Where(o => !o.Key.StartsWith("c_") && !o.Key.StartsWith("cl_"))
                            .ToDictionary(pair => pair.Key, pair => pair.Value);
                    postVar =
                        post.Where(o => !o.Key.StartsWith("c_") && !o.Key.StartsWith("cl_"))
                            .ToDictionary(pair => pair.Key, pair => pair.Value);

                    // test equality of const pre and post
                    foreach (var prePair in preConst)
                    {
                        Assert.AreEqual(prePair.Value, postConst[prePair.Key]);
                    }

                    foreach (var prePair in preLinkedConst)
                    {
                        Assert.AreEqual(prePair.Value, postLinkedConst[prePair.Key]);
                    }
                    // test for negative equality
                    Assert.AreEqual(preVar["int_nr"], -(int) postVar["int_nr"]);
                    Assert.AreEqual(preVar["double_nr"], -(double) postVar["double_nr"]);
                    Assert.AreEqual(preVar["bool"], !(bool) postVar["bool"]);

                    // test for equlity
                    Assert.AreEqual(preVar["date"], postVar["date"]);
                    Assert.AreEqual(preVar["string"], postVar["string"]);
                    Assert.AreEqual(preVar["string_ml"], postVar["string_ml"]);
                    Assert.AreEqual(preVar["undefined_string"], postVar["undefined_string"]);

                    Assert.IsNull(preVar["undefined_string"]);
                    Assert.IsNull(postVar["undefined_string"]);
                }

                //TODO: Table row tests
            });
        }

        [TestMethod]
        public void DataTypes()
        {
            RunMarkdownTestRowByRow(MDTKitHelper.RunVerbose);
        }

        [TestMethod]
        public void ArithmeticExpressions()
        {
            RunMarkdownTestRowByRow((row, index, pre, post) =>
            {
                MDTKitHelper.RunVerbose(row, index, pre, post);
                Assert.AreEqual(row["booleanExpression"], row["booleanResult"]);
                Assert.AreEqual(row["numberExpression"], row["numberResult"]);
            });
        }

        [TestMethod]
        public void LinkedTables()
        {
            RunMarkdownTestRowByRow((row, index, pre, post) =>
            {
                MDTKitHelper.RunVerbose(row, index, pre, post);

                Assert.AreEqual(row["bool_exp"], row["bool_val"]);
                Assert.AreEqual(row["number_exp"], row["number_val"]);
            });
        }

        [TestMethod]
        public void PathSeparators()
        {
            RunMarkdownTestRowByRow((row, index, pre, post) =>
            {
                MDTKitHelper.RunVerbose(row, index, pre, post);

                Assert.AreEqual(Path.DirectorySeparatorChar.ToString(),pre["separator"]);
                var fileInfo = new FileInfo(row["path"]);
                Assert.IsNotNull(fileInfo);              
            });
        }
    }
}