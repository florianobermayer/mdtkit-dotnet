﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MDTKit.Net4.Test.Tests
{
    [TestClass]
    public class MDTKitConstResolvingTest : MDTestCase
    {
        [TestMethod]
        public void ConstantResolving()
        {
            RunMarkdownTestRowByRow((row, i, pre, post) =>
            {
                MDTKitHelper.RunVerbose(row, i, pre, post);
                foreach (var key in pre.Keys)
                {
                    Assert.AreEqual((object) pre[key], (object) post[key]);
                }
            });
        }

        [TestMethod]
        public void ConstantResolvingFail()
        {
            TestUtil.Assert.ExpectExceptionInAggregateExceptionOrDirectly(typeof (MDLinterException),
                "Could not resolve value '$IIIIIUNUSED'",
                () =>
                {
                    RunMarkdownTestRowByRow("ConstantResolvingFail", "ConstantResolvingFail", MDTKitHelper.RunVerbose);
                },
                compareMsgStartsWith: true);
        }
    }
}