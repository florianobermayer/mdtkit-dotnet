#region

#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestAssert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

#endregion

// ReSharper disable PossibleMultipleEnumeration

#endregion

namespace MDTKit.Net4.Test
{
    public static class TestUtil
    {
        public static bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase;
        }

        public static class Assert
        {
            public static void ExpectExceptionInAggregateExceptionOrDirectly(Type exceptionType,
                string expectedExceptionMsg,
                Action action, bool compareMessages = true, bool compareMsgStartsWith = false)
            {
                try
                {
                    ExpectExceptionInAggregateException(exceptionType, expectedExceptionMsg, action, compareMessages,
                        compareMsgStartsWith);
                }
                catch (AssertFailedException e)
                {
                    if (e.Message.StartsWith("Thrown exception '") && e.Message.EndsWith("' is no AggregateException!"))
                    {
                        ExpectException(exceptionType, expectedExceptionMsg, action, compareMessages,
                            compareMsgStartsWith);
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            public static void ExpectExceptionInAggregateException(Type exceptionType, string expectedExceptionMsg,
                Action action, bool compareMessages = true, bool compareMsgStartsWith = false)
            {
                var properlyCatched = false;
                try
                {
                    action();
                }
                catch (AggregateException e)
                {
                    var exceptions = e.InnerExceptions;

                    if (exceptions == null || exceptions.Count == 0)
                    {
                        throw new AssertFailedException("AggregateException has no inner exceptions!");
                    }

                    var applicableExceptions = exceptions.Where(ex => ex.GetType() == exceptionType);

                    foreach (var ex in applicableExceptions)
                    {
                        if (!compareMessages)
                        {
                            properlyCatched = true;
                            break;
                        }

                        if (compareMsgStartsWith)
                        {
                            properlyCatched = ex.Message.StartsWith(expectedExceptionMsg);
                        }
                        else
                        {
                            properlyCatched = expectedExceptionMsg == ex.Message;
                        }

                        if (properlyCatched)
                        {
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new AssertFailedException($"Thrown exception '{e.GetType().Name}' is no AggregateException!");
                }
                if (!properlyCatched)
                {
                    if (expectedExceptionMsg != null && compareMsgStartsWith)
                    {
                        expectedExceptionMsg += "...";
                    }
                    throw new AssertFailedException(
                        $"Expected Exception '{exceptionType.Name}{(expectedExceptionMsg == null ? "" : "(\"" + expectedExceptionMsg + "\")")}' not thrown!");
                }
            }

            public static void ExpectException(Type exceptionType, string expectedExceptionMsg, Action action,
                bool compareMessages = true, bool compareMsgStartsWith = false)
            {
                var properlyCatched = false;
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    UnitTestAssert.AreEqual(exceptionType, e.GetType());
                    if (compareMessages)
                    {
                        if (compareMsgStartsWith)
                        {
                            UnitTestAssert.IsTrue(e.Message.StartsWith(expectedExceptionMsg),
                                $"'{e.Message}' does not start with: '{expectedExceptionMsg}'");
                        }
                        else
                        {
                            UnitTestAssert.AreEqual(expectedExceptionMsg, e.Message);
                        }
                    }
                    properlyCatched = true;
                }
                if (!properlyCatched)
                {
                    if (expectedExceptionMsg != null && compareMsgStartsWith)
                    {
                        expectedExceptionMsg += "...";
                    }
                    throw new AssertFailedException(
                        $"Expected Exception '{exceptionType.Name}{(expectedExceptionMsg == null ? "" : "(\"" + expectedExceptionMsg + "\")")}' not thrown!");
                }
            }

            public static void ExpectException(Type exceptionType, Action action)
            {
                ExpectException(exceptionType, null, action, false, false);
            }

            public static void ExpectAnyOfExceptions(Type[] exceptionTypes, Action action, bool includeDerived = true)
            {
                var exceptions = string.Join(", ", exceptionTypes.Select(type => type.Name).ToArray());
                var properlyCatched = false;
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    bool properException;

                    if (includeDerived)
                    {
                        properException = exceptionTypes.Any(ex => IsSameOrSubclass(ex, e.GetType()));
                        // exclude Assert Exceptions unless they where explicitly listed
                        if ((e.GetType() == typeof (UnitTestAssertException) ||
                             e.GetType() == typeof (AssertFailedException) ||
                             e.GetType() == typeof (AssertInconclusiveException) ||
                             e.GetType() == typeof (InternalTestFailureException)) &&
                            !exceptionTypes.Contains(e.GetType()))
                        {
                            properException = false;
                        }
                    }
                    else
                    {
                        properException = exceptionTypes.Contains(e.GetType());
                    }

                    if (!properException)
                    {
                        throw new AssertFailedException(
                            $"Thrown exception '{e.GetType().Name}' not in list of expected exceptions [{exceptions}]!");
                    }
                    properlyCatched = true;
                }
                if (!properlyCatched)
                {
                    throw new AssertFailedException($"No expected exception [{exceptions}] was thrown!");
                }
            }

            public static void ExpectExceptionWithInnerException(Type innerExceptionType, Action action)
            {
                ExpectExceptionWithInnerException(innerExceptionType, null, action, false, false);
            }

            public static void ExpectExceptionWithInnerException(Type innerExceptionType, string innerExceptionMessage,
                Action action, bool compareMessages = true, bool compareMsgStartsWith = false)
            {
                var properlyCatched = false;
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    if (e.InnerException == null)
                    {
                        throw new AssertFailedException(
                            $"InnerException of exception '{e.GetType().FullName}' is null, expected InnerException of type '{innerExceptionType.FullName}'");
                    }
                    UnitTestAssert.AreEqual(e.InnerException.GetType(), innerExceptionType);
                    if (compareMessages)
                    {
                        if (compareMsgStartsWith)
                        {
                            UnitTestAssert.IsTrue(e.InnerException.Message.StartsWith(innerExceptionMessage),
                                $"'{e.InnerException.Message}' does not start with: '{innerExceptionMessage}'");
                        }
                        else
                        {
                            UnitTestAssert.AreEqual(innerExceptionMessage, e.InnerException.Message);
                        }
                    }
                    properlyCatched = true;
                }
                if (!properlyCatched)
                {
                    if (innerExceptionMessage != null && compareMsgStartsWith)
                    {
                        innerExceptionMessage += "...";
                    }
                    throw new AssertFailedException(
                        $"Expected InnerException '{innerExceptionType.Name}{(innerExceptionMessage == null ? "" : "(\"" + innerExceptionMessage + "\")")}' not thrown!");
                }
            }

            public static void ExpectExceptionOrDerived(Type exceptionType, Action action)
            {
                var properlyCatched = false;
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    if (!IsSameOrSubclass(exceptionType, e.GetType()))
                    {
                        throw new AssertFailedException(
                            string.Format(
                                "Thrown exception '{0}' not type of or subclass of expected exception '{1}'!",
                                e.GetType().Name, exceptionType.Name));
                    }
                    UnitTestAssert.IsTrue(IsSameOrSubclass(exceptionType, e.GetType()));
                    properlyCatched = true;
                }
                if (!properlyCatched)
                {
                    throw new AssertFailedException(string.Format("Expected Exception '{0}' not thrown!",
                        exceptionType.Name));
                }
            }

            public static void Contains<T>(T containingItem, IEnumerable<T> collection)
            {
                if (!collection.Contains(containingItem))
                {
                    throw new AssertFailedException(string.Format("Collection '{0}' does not contain item '{1}'!",
                        collection, containingItem));
                }
            }

            public static void AreEqualCollections(ICollection collection1, ICollection collection2,
                List<ICollection> referenceStore = null)
            {
                try
                {
                    if (referenceStore == null)
                    {
                        referenceStore = new List<ICollection>();
                    }

                    if (referenceStore.Contains(collection1))
                    {
                        return;
                    }
                    referenceStore.Add(collection1);

                    if (collection1 is IDictionary)
                    {
                        var e1 = ((IDictionary) collection1).GetEnumerator();
                        var e2 = ((IDictionary) collection2).GetEnumerator();

                        while (e1.MoveNext() && e2.MoveNext())
                        {
                            UnitTestAssert.AreEqual(e1.Key, e2.Key);

                            if (e1.Value is ICollection)
                            {
                                AreEqualCollections((ICollection) e1.Value, (ICollection) e2.Value, referenceStore);
                            }
                            else
                            {
                                UnitTestAssert.AreEqual(e1.Value, e2.Value);
                            }
                        }
                    }
                    else
                    {
                        var e1 = collection1.GetEnumerator();
                        var e2 = collection2.GetEnumerator();

                        while (e1.MoveNext() && e2.MoveNext())
                        {
                            if (e1.Current is ICollection)
                            {
                                AreEqualCollections((ICollection) e1.Current, (ICollection) e2.Current, referenceStore);
                            }
                            else
                            {
                                UnitTestAssert.AreEqual(e1.Current, e2.Current);
                            }
                        }
                    }
                }
                catch (AssertFailedException e)
                {
                    throw new AssertFailedException(
                        string.Format("Collection '{0}' and '{1}' are not equal!", collection1, collection2), e);
                }
            }
        }
    }
}