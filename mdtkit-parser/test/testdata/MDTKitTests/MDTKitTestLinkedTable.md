$IMPORT_ONLY$

A table which is to be imported in a linked table test.

| teststring:string | bool_exp:boolean | bool_val:boolean | number_exp:number | number_val:number |
|:------------------|:----------------:|:----------------:|:-----------------:|:-----------------:|
| "test string"     |       true       |       true       |         1         |         1         |
| ""                |  true AND false  |      false       |       1+4-7       |        -2         |
