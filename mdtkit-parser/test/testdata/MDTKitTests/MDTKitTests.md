Markdown Test Kit Specification
===============================

Markdown Test Kit (MDTKit) is an interface for using markdown (.md) files for defining test specification and data for test environments cross-platform.

MDTKit supports following concepts:

-	Linking between markdown files
-	Multiline variables and constants
-	Data Tables (traversed row by row)
-	Pre-Constants
-	Post-Constants

It basically consists of:

-	a markdown test file **describing the test** and **providing test data**
-	a test implementation deriving from `MDTestCase` class

### Describing the Markdown file:

-	The **markdown file** must be named after the test class name.
-	**Linking** files simply replaces the link placeholder with the content of the declared file and thus can be used anywhere in code like this:

	```
	import <MDTKitTestLinkedFile.md>
	```

-	Each **test** is described in a section starting with an (alt) H1 Header.

-	**Constants** can be defined anywhere and are valid globally within all linked files. They can be used in each test section within **data tables** and **pre** or **post constants**:

	```
	const NUMBER                      = 42; // can be of int or double value.
	const DOUBLE_NUMBER                = -4.4512;
	const BOOLEAN                     = true;
	const STRING                      = "this is an awesome string."; // strings must be surrounded by quotes. Any inner quotes will be retained.
	const STRING_ML                   = "this is another awesome string
	                                     it is even multiline!";
	const DATE                        = "2015-04-27T21:43:53Z"; // can optionally be surrounded by quotes.
	const NULL                        = [null]; // can also be null without brackets.
	```

-	A **data table** is only valid in it's test section and has a **header row** defining **variable names** and their **data types** like so:

	| FirstColumnVariableName:number | NthColumnVariableName:string |
	|--------------------------------|------------------------------|
	| 1                              | $STRING                      |
	| $NUMBER                        | "some other string           |

**Note:** The above table represents a global table (as it is defined before any test case). If a TestCase does not define a table, the global table is used automatically

-	**Pre** and **post constants** are also only valid in section scope and help reducing data table sizes with recurring entries:

	```
	pre boolExpression:boolean               = true;
	pre stringExpression:string              = $STRING;
	post dateExpression:date                 = $DATE;
	post undefinedStringExpression:string    = null;
	```

### General MDTKit Test Coding Procedure :

1.	Derive from test class `MDTestCase` and name class according to markdown test file
2.	Write Test Methods according to their markdown test file sections.
	-	**.Net:** Call `RunMarkdownTestRowByRow(Action<Dictionary<string, object>, int, Dictionary<string, object>, Dictionary<string, object>> run)` and provide an `Action` holding test logic.
	-	**TODO: other platforms**

DataTypes
---------

Following datatypes are supported:

```
pre an_int_number:number          = 42;
pre a_double_number:number         = 3.562;
pre a_bool:boolean                = true;
pre a_string:string               = "wow what a cool string.
                                    its even multiline!";
post a_date:date                  = "2015-04-27T21:43:53Z"; // in T-Z format
post an_undefined_string:string   = null; // basically every data type can be null (platform dependant!)
```

**WARNING: Do not forget `;` after each entry!**

**Note:** The above section already represents a test case and therefore it requires a table:

| var:number |
|:----------:|
|     0      |

ArithmeticExpressions
---------------------

The MDTKit also supports simple aritmetic expressions instead of data values:

### Boolean Arithmetic:

All boolean arithmetic expressions JavaScript can parse are valid.

Hence, **"&&"**, **"||"** and **"!"** have to be substituted with **"AND"**, **"OR"** and **"NOT"**.

### Number Arithmetic:

All numeric expressions JavaScript can parse (including JavaScript function calls) are valid.

For bitwise operations use **"AND"** and **"OR"**.

### String Arithmetic:

Strings can be any JavaScript method which will then be evaluated.

In multiline strings, do not forget to omit ";" character.

```
const JS = (Math.random() > 0.5? (3+2): (8-3));

const AE_STRING = "'das ist ein string'";
const AE_CHAR = "'e'";
const LEFTSHIFT = <<;

pre random:number = Math.random();
pre string:string = $STRING;

pre javascript:number = $JS;

post generatedTimeStampString:string = "new Date().getTime().toString()";
```

| booleanExpression:boolean          | booleanResult:boolean | numberExpression:number          | numberResult:number |
|:-----------------------------------|:----------------------|----------------------------------|---------------------|
| true AND true                      | true                  | 4+3                              | 7                   |
| true AND false                     | false                 | 4*4                              | 16                  |
| true AND (false AND true)          | false                 | Math.pow(4,3)                    | 64                  |
| true AND true == !(false OR false) | true                  | 0-5.6                            | -5.6                |
| !!false                            | false                 | $AE_STRING.lastIndexOf($AE_CHAR) | 8                   |
| true AND !true                     | false                 | 1 $LEFTSHIFT 3                   | 8                   |
| true AND (!false OR false)         | true                  | Math.sqrt(4)                     | 2                   |
| false ^ true                       | true                  | $JS                              | 5                   |
| false ^ false                      | false                 | 7 L_AND ~2                       | 5                   |

LinkedTables
------------

Tables in tests can also be linked via the import directive like so:

```
import <MDTKitTestLinkedTable.md>
```

Make sure this import statement is correctly placed within a Test definition (has it's own H2 Heading)

Further MDTKitTests
===================

VerboseSmokeTest
----------------

This test generally tests the parsing procedure by simply using all of the concepts the MDTKit provides.

#### Test Procedue:

1.	The test will simply test matching every pre constant with post constants's equality coming from global and global linked constants.
2.	Some pre and post constants with local values will then be matched against inversive equality (number, boolen) and equality (date, undefined string)
3.	Finally the table data will be used to test.. **TODO: Do some testing with table data**

##### Pre constants using global constants:

```
pre c_int_nr:number                   = $NUMBER;
pre c_double_nr:number                = $DOUBLE_NUMBER;
pre c_bool:boolean                    = $BOOLEAN;
pre c_string:string                   = $STRING;
pre c_string_ml:string                = $STRING_ML;
pre c_date:date                       = $DATE;
pre c_undefined_string:string         = $NULL;
```

##### Post constants using global constants:

```
post cl_int_nr:number                   = $LINKED_NUMBER;
post cl_double_nr:number                = $LINKED_DOUBLE_NUMBER;
post cl_bool:boolean                    = $LINKED_BOOLEAN;
post cl_string:string                   = $LINKED_STRING;
post cl_string_ml:string                = $LINKED_STRING_ML;
post cl_date:date                       = $LINKED_DATE;
post cl_undefined_string:string         = $LINKED_NULL;
```

##### Pre constants using global linked constants:

```
pre cl_int_nr:number                   = $LINKED_NUMBER;
pre cl_double_nr:number                = $LINKED_DOUBLE_NUMBER;
pre cl_bool:boolean                    = $LINKED_BOOLEAN;
pre cl_string:string                   = $LINKED_STRING;
pre cl_string_ml:string                = $LINKED_STRING_ML;
pre cl_date:date                       = $LINKED_DATE;
pre cl_undefined_string:string         = $LINKED_NULL;
```

##### Post constants using global linked constants:

```
post c_int_nr:number                   = $NUMBER;
post c_double_nr:number                = $DOUBLE_NUMBER;
post c_bool:boolean                    = $BOOLEAN;
post c_string:string                   = $STRING;
post c_string_ml:string                = $STRING_ML;
post c_date:date                       = $DATE;
post c_undefined_string:string         = $NULL;
```

##### Pre constants using local values:

```
pre int_nr:number                   = 3;
pre double_nr:number                = 6.14278;
pre bool:boolean                    = false;
pre string:string                   = "a string";
pre string_ml:string                = "a multiline
                                         string";
pre date:date                       = "2015-06-10T19:52:51Z";
pre undefined_string:string         = [null];
```

##### Post constants using local values:

```
post int_nr:number                   = -3;
post double_nr:number                = -6.14278;
post bool:boolean                    = true;
post string:string                   = "a string";
post string_ml:string                = "a multiline
                                         string";
post date:date                       = '2015-06-10T19:52:51Z';
post undefined_string:string         = null;
```

#### Data Table:

| number_val:number | boolean_val:boolean | date_val:date        | string_val:string                                       |
|-------------------|---------------------|----------------------|---------------------------------------------------------|
| 0                 | false               | 2015-06-10T19:52:51Z | "yep, a string"                                         |
| 0.5               | true                | [null]               | $STRING_ML                                              |
| -3                | [null]              | [null]               | "\"string with quotes\""                                |
| -0.4              | [null]              | [null]               | "\"string with only one quote"                          |
| [null]            | null                | null                 | 'a string with single quotes'                           |
| null              | null                | null                 | 'a string with single quotes and a sole single quote\'' |
| null              | null                | null                 | null                                                    |

**Note: This table should check null parsing behavior, too.**

PathSeparators
--------------

This test tests the platform wide correct substitution of path separators within testdata.

```
const SEP = require('path').sep;
pre separator:string = $SEP;

```

| path:string                       |
|:----------------------------------|
| $SEP + "base" + $SEP + "whatever" |

Negative Tests
==============

Negative tests are found in separate test files.
