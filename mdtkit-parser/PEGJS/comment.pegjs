
// TODO: define it explicitly (not as inversion of other blocks - maybe use stripped MarkDown grammer (without H2 parsing))
Comment "comment"
 = !(TestTitle / Codeblock / Table) CommentText !(TestTitle / Codeblock / Table)
 {
   return buildValue(text, Type.COMMENT, location, path);
 }

/*
    The CommentText can consist of any char sequence (besides newline)
*/
CommentText
 = (!NEWLINE .)* // TODO: check if that is enough (NEWLINE rule has far more newline definitions)
