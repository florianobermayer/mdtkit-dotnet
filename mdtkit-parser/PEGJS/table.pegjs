Table
 = headerRow:HeaderRow NEWLINE formatRow:FormatRow tableContent:TableContent
{

  const table = {
   "header": headerRow,
   "formatRow": formatRow,
   "content": tableContent
  };

  const validation = validateTable(table);
  if(!validation.valid){
    raise(validation.error, location);
  }
  return validation.table;
}


HeaderRow
= headerRow:(TABLEDELIMITER HeaderCell)+ TABLEDELIMITER
{
  return parseToList(headerRow,1);
}

HeaderCell
 = HeaderCell_ENTER header:HeaderContent HeaderCell_LEAVE
 {
   return header;
 }

HeaderCell_ENTER
 = w:__
 {
   check("HeaderCell_ENTER", location);
   return w;
 }

HeaderCell_LEAVE
 = w:__
 {
   check("HeaderCell_LEAVE", location);
   return w;
 }

HeaderContent
 = title:RowTitle ":" type:ValidType
 {
   return buildValue({"title": title, "type": type}, Type.HEADER, location, path);
 }

RowTitle
 = ValidTypeName

FormatRow
 = formatRow:(TABLEDELIMITER FormatRowCell)+ TABLEDELIMITER
{
  return parseToList(formatRow,1);
}

FormatRowCell
 = _ format:(TABLEFORMAT? TABLEFORMATROWCHAR+ TABLEFORMAT?) _
{
  return flattenArray(format).join("");
}

TableContent
  = tableContent:(NEWLINE TableContentRow)+
  {
    const content = parseToList(tableContent, 1);
    return content;
  }


TableContentRow
 = contentRow:(TABLEDELIMITER ContentCell)+ TABLEDELIMITER
  {
    return parseToList(contentRow, 1);
  }

ContentCell
  =  ContentCell_ENTER content:Content ContentCell_LEAVE
{
  return content;
}


ContentCell_ENTER
 = w:__
 {
   check("ContentCell_ENTER", location);
   return w;
 }

ContentCell_LEAVE
 = w:__
 {
   check("ContentCell_LEAVE", location);
   return w;
 }


Content
 = val:ValidSingleLineLiteral &(_ TABLEDELIMITER) { return val; }
 / val:UntypedLiteral { return val; }


TABLEDELIMITER
 = "|"

TABLEFORMAT
 = ":"

TABLEFORMATROWCHAR
 = "-"
