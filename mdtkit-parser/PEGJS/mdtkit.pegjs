
{
    const pathModule = require('path');
    const MDParser = require(pathModule.join(__dirname, '../', 'mdtkit-parser')).instance;
    const utils = require(pathModule.join(__dirname, '../', 'utils'));
    const errorManager = MDParser.ErrorManager;

    const Type = require(pathModule.join(__dirname, '../', 'literal-type'));

    const parseToList = (arg, idx) => utils.parseToList(arg, idx);
    const flattenArray = (arr) => utils.flattenArray(arr);
    const validateTable =  (table) =>  utils.validateTable(table);
    const checkDuplicateTestCases =  (testCases) =>  utils.checkDuplicateTestCases(testCases, errorManager);
    const debug = (obj, location, type) => utils.debug(obj, location, type);

    const _ = require("lodash");
    // used to check sensible locations entry and exit
    const check = (blockName, location) => errorManager.check(blockName, location);
    // used to raise an error
    const raise = (message, location) => errorManager.raise(message, location);
    //const importParser = require(pathModule.join(__dirname, 'importParser');

    const path = () => MDParser.CurrentFilePath;
    const parseGlobalContentRegion = (list) => utils.parseGlobalContentRegion(list);
    const buildValue = (value, type, location, path) => utils.buildValue(value, type, location, path);
    const createDate = (parsedDate) => utils.createDate(parsedDate);
}

/*
    Starting point. Needs to have a Test and an EOF
*/
TestFile
 = SPACING? test:Test SPACING?
 {
   return test;
 }

/*
  Used by the linking process
*/
ImportGrammar
 = content:ImportGlobalContentRegion SPACING? .* EOF
 {
   return content;
 }

/*
Used for arithmetics validation
*/
ValidArithmeticalResolvedStatement
 = ValidCodeblockLiteral


ImportGlobalContentRegion
 = globalContentFirst: GlobalContent globalContents:(SPACING GlobalContent)*
 {
    if(globalContentFirst === undefined || globalContentFirst === null){
       return parseGlobalContentRegion(null);
    }
    if(globalContents === undefined || globalContents === null){
      return parseGlobalContentRegion([globalContentFirst]);
    }
    const globalContentList = parseToList(globalContents, 1);
    globalContentList.push(globalContentFirst)
    return parseGlobalContentRegion(globalContentList);
 }
  / SPACING?
  {
    return parseGlobalContentRegion(null);
  }

SPACING
 = NEWLINE+
 {return " ";}

 // @append "comment.pegjs"

 /*
     Global content does not belong to a specific test case.
     It can consist of Comments or Codeblocks or a global table
 */
GlobalContent
 = codeblock:Codeblock { return {"codeblock" : codeblock}; }
 / table: GlobalTable {return {"table" : table}; }
 / comment:Comment { return {"comment" : comment}; }

GlobalTable
 = Table

/*
    GlobalContentRegion also provides spacing in between and to the TestCase
*/
GlobalContentRegion
 = globalContent:((GlobalContent SPACING)+
   / SPACING?)
   {
     //debug(globalContent, location, "GlobalContentRegion");
      if(globalContent === null){
        return parseGlobalContentRegion(null);
      }
      const globalContentList = parseToList(globalContent, 0);
    //  debug(globalContentList, location, "GlobalContentRegion-ParsedToList");

      return parseGlobalContentRegion(globalContentList);
   }

/*
    A Test consists of a GlobalContentRegion and multiple TestCases
*/
Test
 = globalContent: GlobalContentRegion testCases:(TestCase+)
 {

   const result = {
     "globalContent": globalContent,
     "testCases": testCases
  };

   checkDuplicateTestCases(result.testCases);
   return result;
 }


/*
    The TestCase consists of a TestTitle and multiple SPACED TestContents
*/
TestCase "test case"
= SPACING? title:TestTitle testContents:(SPACING TestContent)*
 {
  const result = {
    "title" : title,
    "comments" : [],
    "codeblocks" : [],
    "table" : null,
    "location" : location(),
    "path" : path()
  };

  const testContentsList = parseToList(testContents, 1);

  testContentsList.forEach(function(elem){
    if(elem["codeblock"] !== undefined){
      result["codeblocks"].push(elem["codeblock"]);
    }
    if(elem["comment"] !== undefined){
      result["comments"].push(elem["comment"]);
    }
    if(elem["table"] !== undefined){
      if(result["table"] !== null){
        raise("Only a single table is allowed!", location);
      }
      result["table"] = elem["table"];
    }
  });
  return result;
 }

/*
    TestContent is content but specific to a test holding a testTable
*/
TestContent
 = codeblock:Codeblock { return {"codeblock": codeblock}; }
 / table:Table { return { "table": table }; }
 / comment:Comment { return {"comment": comment}; }


/*
    The TestTitle contains of a ValidTypeName , a NEWLINE and a number of "-" matching the length of the Title
*/
TestTitle "test title" //TODO: add block check here
 = title:ValidTypeName NEWLINE line:TitleUnderline NEWLINE
{
  var result = title;
  if(line.join("").count != result.count){
    raise("TestTitle underlining does not fit title!", location);
  }
  return result;
}

TitleUnderline "Title Underline"
 = MINUS+


/*
A ValidTypeName is a string which is allowed to be a name in common programming languages
*/
ValidTypeName "type name"
 = typename:([a-zA-Z_]+ [a-zA-Z0-9_]*)
 {
   var result = typename[0].join("") + typename[1].join("");
   return result;
 }

// @append "codeblock.pegjs"
// @append "literals.pegjs"



ValidType
 = TYPE_STRING
 / TYPE_BOOLEAN
 / TYPE_DATE
 / TYPE_NUMBER
 / TYPE_UNSUPPORTED



// @append "table.pegjs"


/* ----- 1. Base Types ----- */

TYPE_STRING "type_string"
 = "string"

TYPE_BOOLEAN "type_boolean"
 = "boolean"

TYPE_DATE "type_date"
 = "date"

TYPE_NUMBER "type_number"
 = "number"

TYPE_UNSUPPORTED "type_unsupported"
 = (!" |" !NEWLINE .)*
 {
   raise("Unsupported type: '" + text() + "'", location);
 }

NEWLINE "newline"
 = "\r\n"
 / "\n"
 / "\r"
 / "\u2028"
 / "\u2029"

EOF "end of file"
 = !.

CONST
 = "const"

PRE
 = "pre"

POST
 = "post"

EQUALS
 = "="

COLON
 = ":"

SEMICOLON
 = ";"

CODEQUOTES
 = _ "```" _

NULLTOKEN
 = "null"
 / "NULL"
 / "[null]"


MINUS
 = "-"
