/**
 * Created by Florian on 15.04.2016.
 */
'use strict';

const _ = require("lodash");
const Utils = require("./utils.js");

const ENTER = "_ENTER";
const LEAVE = "_LEAVE";

class ErrorManager {

    /**
     *
     * @param {String} message
     * @param {Location | Function} [location]
     * @param {String} [filepath]
     */
    raise(message, location, filepath){

        if(!_.isString(filepath)){
            filepath = this._parserInstance.CurrentFilePath;
        }
        if(_.isFunction(location)){
            location = location();
        }

        if(!_.isObject(location)){
            location = {"start": {"offset": 0,"line": 0,"column": 0}, "end": {"offset": 0,"line": 0,"column": 0}};
        }

        if(!_.isString(filepath)){
            path = "";
        }

        const errorObj = {
            "message": message,
            "location": location,
            "path" : filepath
        };

        if(_.findIndex(this._errorsArr, errorObj) === -1){
            this._errorsArr.push(errorObj);
        }
    }

    check(blockName, location){
        if(_.endsWith(blockName, ENTER)){
            this._enterBlock(blockName, location);
        }else if(_.endsWith(blockName, LEAVE)){
            this._leaveBlock(blockName);
        }else{
            throw "Not a valid block definition."
        }
    }

    /**
     * @private
     */
    _enterBlock(blockName, location){

        if(typeof location === "function"){
            location = location();
        }
        const block = {
            "blockName": blockName,
            "location": location,
            "path" : this._parserInstance.CurrentFilePath
        };

        this._blockStack.push(block);
    }

    /**
     * @private
     */
    _leaveBlock(blockName){
        const lastIndex = _.findLastIndex(this._blockStack, elem =>
        {
            return _.replace(elem["blockName"], ENTER, "") === _.replace(blockName, LEAVE, "") && elem["filepath"] === this._parserInstance.CurrentFilePath;
        });

        this._blockStack.splice(lastIndex, 1)
    }

    /**
     *
     * @param {MDTKitParser} parserInstance
     */
    constructor(parserInstance) {
        this._parserInstance = parserInstance;
        this._blockStack = [];
        this._errorsArr = [];
    }

    get stack(){
        return this._blockStack;
    }

    get errors(){
        return this._errorsArr;
    }

    /**
     *
     * @returns {boolean}
     */
    hasErrors(){
        return !_.isEmpty(this.stack) || !_.isEmpty(this.errors);
    }

    deduplicate(){
        this._blockStack = Utils.arrUnique(this._blockStack);
        this._errorsArr = Utils.arrUnique(this._errorsArr);
    }
}

module.exports = ErrorManager;
