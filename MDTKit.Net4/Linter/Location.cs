﻿// ReSharper disable InconsistentNaming

using System;
using Newtonsoft.Json;

namespace MDTKit.Linter
{
    public class Location
    {
        public Location(LocationEntry start, LocationEntry end)
        {
            Start = start;
            End = end;
        }

        public LocationEntry Start { get; }
        public LocationEntry End { get; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static Location FromDynamic(dynamic location)
        {
            try
            {
                return new Location
                    (
                    new LocationEntry(
                        (int) location.start.column,
                        (int) location.start.line,
                        (int) location.start.offset
                        ),
                    new LocationEntry(
                        (int) location.end.column,
                        (int) location.end.line,
                        (int) location.end.offset
                        )
                    );
            }
            catch (Exception)
            {
                return new Location
                    (
                    new LocationEntry(
                        (int) location.Start.Column,
                        (int) location.Start.Line,
                        (int) location.Start.Offset
                        ),
                    new LocationEntry(
                        (int) location.End.Column,
                        (int) location.End.Line,
                        (int) location.End.Offset
                        )
                    );
            }
        }

        public class LocationEntry
        {
            public LocationEntry(int column, int line, int offset)
            {
                Column = column;
                Line = line;
                Offset = offset;
            }

            public int Column { get; }
            public int Line { get; }

            public int Offset { get; }
        }
    }
}