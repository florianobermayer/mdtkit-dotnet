﻿namespace System.Runtime.CompilerServices
{
#if NETFX_CORE
#else
    /// <summary>
    ///     TODO: Remove Class when upgrading to .Net >= 4.5
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter)]
    public class CallerMemberNameAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class CallerFilePathAttribute : Attribute
    {
    }
#endif
}