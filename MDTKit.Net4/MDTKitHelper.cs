﻿#region

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace MDTKit
{
    public static class MDTKitHelper
    {
        public static void RunVerbose(Dictionary<string, dynamic> row, int index, Dictionary<string, dynamic> pre,
            Dictionary<string, dynamic> post)
        {
            var undefinedTypeName = "undefined";
            var minTypeSize = undefinedTypeName.Length;
            var tablefmt = "   |";
            var headings = row.Select(pair =>
            {
                var typename = pair.Value?.GetType().Name ?? undefinedTypeName;
                if (typename == typeof (int).Name || typename == typeof (double).Name)
                {
                    typename = "number";
                }
                else if (typename == typeof (DateTime).Name)
                {
                    typename = "date";
                }

                typename = typename.ToLower();
                var hfmt = "{0}:{1,-" + Math.Max(minTypeSize, typename.Length) + "}";
                return string.Format(hfmt, pair.Key, typename);
            }).ToList();
            for (var i = 0; i < headings.Count; i++)
            {
                tablefmt += " {" + i + "," + -(headings[i].Length + 1) + "}|";
            }

            if (index == 0)
            {
                var fmt = "   {0,-20} {1}";
                Console.WriteLine("\nPre-Constants:");
                foreach (var entry in pre)
                {
                    Console.WriteLine(string.Format(fmt, entry.Key, entry.Value));
                }

                Console.WriteLine("\nPost-Constants:");
                foreach (var entry in post)
                {
                    Console.WriteLine(string.Format(fmt, entry.Key, entry.Value));
                }

                Console.WriteLine("\nTable:");
                // ReSharper disable once CoVariantArrayConversion
                Console.WriteLine(tablefmt, headings.ToArray()); // headings
                // ReSharper disable once CoVariantArrayConversion
                Console.WriteLine("   |" +
                                  string.Concat(Enumerable.Repeat("-",
                                      string.Format(tablefmt, headings.ToArray()).Length - 5)) + "|"); // delimiter line
            }

            // ReSharper disable once CoVariantArrayConversion
            var res = string.Format(tablefmt,
                row.Values.Select(
                    o => o?.ToString().Replace("\t", "\\t").Replace("\r\n", "\\n").Replace("\n", "\\n") ?? "null")
                    .ToArray());
            Console.WriteLine(res); // values
        }
    }
}