using System.Collections.Generic;

namespace MDTKit
{
    // ReSharper disable once InconsistentNaming
    public interface IMDTestTable
    {
        List<string> Headings { get; }
        List<Dictionary<string, object>> Rows { get; }
    }
}