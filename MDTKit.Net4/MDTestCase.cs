﻿#region

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

#endregion

namespace MDTKit
{
    // ReSharper disable once InconsistentNaming
    public abstract class MDTestCase
    {
        /// <summary>
        ///     Used to provide a custom testData base folder. If ommitted, ../../../testData is used (relative to CWD)
        /// </summary>
        protected string TestDataFolderPath
        {
            set { MDTestData.TestDataBasePath = value; }
        }


        protected void RunMarkdownTestRowByRow(string testname, string testFilename,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> run)
        {
            var testData = MDTestData.Create(testFilename, testname);
            var table = testData.MDTestTable;
            for (var rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
            {
                run(table.Rows[rowIndex], rowIndex, testData.PreConstants, testData.PostConstants);
            }
        }

        /// <summary>
        ///     Starts execution of test run.
        /// </summary>
        /// <param name="testname">the name of the test searched in markdown test file named by its test class.</param>
        /// <param name="run">
        ///     Run action:
        ///     first param:    current row of the markdown table
        ///     second param:   the row's index of the markdown table
        ///     third param:    preConstants
        ///     forth param:    postConstants
        /// </param>
        /// <param name="testClassType">the type of the test class matching the markdown file name</param>
        protected void RunMarkdownTestRowByRow(string testname, Type testClassType,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> run)
        {
            var testData = MDTestData.Create(testname, testClassType);
            var table = testData.MDTestTable;
            for (var rowIndex = 0; rowIndex < table.Rows.Count; rowIndex++)
            {
                run(table.Rows[rowIndex], rowIndex, testData.PreConstants, testData.PostConstants);
            }
        }

        /// <summary>
        ///     Starts execution of test run.
        /// </summary>
        /// <param name="testname">the name of the test searched in markdown test file named by its test class.</param>
        /// <param name="run">
        ///     Run action:
        ///     first param:    current row of the markdown table
        ///     second param:   the row's index of the markdown table
        ///     third param:    preConstants
        ///     forth param:    postConstants
        /// </param>
        protected void RunMarkdownTestRowByRow(string testname,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> run)
        {
            RunMarkdownTestRowByRow(testname, GetType().Name, run);
        }

        /// <summary>
        ///     Starts excecution of test run. Uses calling method name (which ideally is the testname) automatically. (param
        ///     callingMethod should be ommitted)
        /// </summary>
        /// <param name="run"></param>
        /// <param name="callingMethod"></param>
        protected void RunMarkdownTestRowByRow(
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> run,
            [CallerMemberName] string callingMethod = "")
        {
            RunMarkdownTestRowByRow(callingMethod, run);
        }
    }
}