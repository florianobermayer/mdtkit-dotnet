using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MDTKit
{
    public static class MDTestRunner
    {
        public static void Run(
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> execute,
            bool failImmediatelyWhenTestRowFails = true, bool verbose = false,
            [CallerFilePath] string callingClassName = "", [CallerMemberName] string callingMethodName = "")
        {
            // ReSharper disable ExplicitCallerInfoArgument
            Run(null, execute, null, failImmediatelyWhenTestRowFails, verbose, callingClassName, callingMethodName);
            // ReSharper restore ExplicitCallerInfoArgument
        }

        public static void Run(
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> setup,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> execute,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> teardown,
            bool failImmediatellyWhenTestRowFails = true, bool verbose = false,
            [CallerFilePath] string callingClassName = "", [CallerMemberName] string callingMethodName = "")
        {
            if (callingClassName.EndsWith(".cs"))
            {
                callingClassName = Path.GetFileNameWithoutExtension(callingClassName);
            }

            var mdTestData = MDTestData.Create(callingClassName, callingMethodName);

            if (failImmediatellyWhenTestRowFails)
            {
                // fail test immediately after one MDT Test Row failes.
                // run test with mdtestdata
                for (var i = 0; i < mdTestData.MDTestTable.Rows.Count; i++)
                {
                    RunTestSequence(mdTestData.MDTestTable.Rows[i], i, mdTestData.PreConstants, mdTestData.PostConstants,
                        setup, execute, teardown, verbose);
                }
            }
            else
            {
                // continue rest of lines and fail after whole table if any row failed.
                var throwExceptions = new List<Exception>();
                // run test with mdtestdata
                for (var i = 0; i < mdTestData.MDTestTable.Rows.Count; i++)
                {
                    try
                    {
                        RunTestSequence(mdTestData.MDTestTable.Rows[i], i, mdTestData.PreConstants,
                            mdTestData.PostConstants, setup, execute, teardown, verbose);
                    }
                    catch (Exception e)
                    {
                        throwExceptions.Add(e);
                    }
                }

                if (throwExceptions.Count > 0)
                {
                    var separatorLine =
                        $"{Environment.NewLine}{Environment.NewLine}{string.Concat(Enumerable.Repeat("#", 80))}{Environment.NewLine}{Environment.NewLine}";
                    var exmsg =
                        $"One or more Exceptions occurred:{Environment.NewLine}{Environment.NewLine}{Environment.NewLine}";
                    exmsg += string.Join(separatorLine, throwExceptions.Select(exception => exception.ToString()));
                    exmsg += separatorLine;
                    exmsg +=
                        $"First Exception's Stacktrace:{Environment.NewLine}{Environment.NewLine}{Environment.NewLine}";
                    throw new AggregateException(exmsg, throwExceptions);
                }
            }
        }

        private static void RunTestSequence(Dictionary<string, dynamic> row, int index, Dictionary<string, dynamic> pre,
            Dictionary<string, dynamic> post,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> setup,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> execute,
            Action<Dictionary<string, dynamic>, int, Dictionary<string, dynamic>, Dictionary<string, dynamic>> teardown,
            bool verbose)
        {
            Exception testException = null;
            try
            {
                if (setup != null)
                {
                    if (verbose)
                    {
                        Console.WriteLine($"--- --- --- --- Setup line [{index}] ... --- --- --- ---");
                    }
                    try
                    {
                        setup(row, index, pre, post);
                    }
                    catch (MDTestSetupException)
                    {
                        throw;
                    }
                    catch (Exception e)
                    {
                        throw new MDTestSetupException(e, index);
                    }
                }

                if (verbose)
                {
                    Console.WriteLine($"\n--- --- --- --- Execute line [{index}] ... --- --- --- ---");
                }
                execute(row, index, pre, post);
            }
            catch (MDTestSetupException e)
            {
                testException = e;
            }
            catch (Exception e)
            {
                testException = new MDTestExcecutionException(e, index);
            }

            if (teardown != null)
            {
                if (verbose)
                {
                    Console.WriteLine($"\n--- --- --- --- TearDown line [{index}] ... --- --- --- ---");
                }
                try
                {
                    teardown(row, index, pre, post);
                }
                catch (Exception e)
                {
                    if (testException == null)
                    {
                        throw new MDTestTearDownException(e, index);
                    }
                    if (testException is MDTestSetupException)
                    {
                        throw new AggregateException("SETUP AND TEAR DOWN FAILED!", testException, e);
                    }
                    throw new AggregateException("TEST AND TEAR DOWN FAILED!", testException,
                        new MDTestTearDownException(e, index));
                }
            }

            if (testException != null)
            {
                throw testException;
            }
        }
    }
}