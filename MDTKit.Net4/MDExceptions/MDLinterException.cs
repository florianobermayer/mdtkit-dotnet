﻿using System;
using MDTKit.Linter;

namespace MDTKit
{
    // ReSharper disable once InconsistentNaming
    public class MDLinterException : Exception
    {
        public MDLinterException(string message, Location location, string path)
            : base($"{message}\n\nLocation:\n{location}\n\nPath:\n{path}")
        {
            Location = Location.FromDynamic(location);
            Path = path;
        }

        public Location Location { get; }
        public string Path { get; }
    }

    // ReSharper disable once InconsistentNaming
    public class MDLinterStackException : MDLinterException
    {
        public MDLinterStackException(string message, Location location, string path)
            : base(FormatBlockMessage(message), location, path)
        {
        }

        private static string FormatBlockMessage(string codeblockMessage)
        {
            var index = codeblockMessage.IndexOf("_", StringComparison.InvariantCulture);
            if (index != -1)
            {
                return $"Malformatted area: '{codeblockMessage.Substring(0, index)}'";
            }
            return "Malformatted area: 'Unknown area'";
        }
    }
}