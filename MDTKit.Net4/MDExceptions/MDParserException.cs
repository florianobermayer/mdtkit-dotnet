﻿#region

using System;

#endregion

namespace MDTKit
{
    // ReSharper disable once InconsistentNaming
    public class MDParserException : Exception
    {
        public MDParserException(string message) : base(message)
        {
        }

        public MDParserException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}