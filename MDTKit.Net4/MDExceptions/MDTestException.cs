﻿#region

using System;

#endregion

namespace MDTKit
{
    internal abstract class MDTestExceptionBase : Exception
    {
        protected MDTestExceptionBase(string title, string message, Exception exception, int rowIndex = -1)
            : base($"{FormatWithRowIndex(title, rowIndex)}[{message}]{Environment.NewLine}", exception)
        {
        }

        protected MDTestExceptionBase(string title, Exception exception, int rowIndex = -1)
            : base(FormatWithRowIndex(title, rowIndex), exception)
        {
        }

        protected MDTestExceptionBase(string title, string message, int rowIndex = -1)
            : base($"{FormatWithRowIndex(title, rowIndex)}[{message}]{Environment.NewLine}")
        {
        }

        private static string FormatWithRowIndex(string title, int index)
        {
            title = $"{title}{Environment.NewLine}";
            return index != -1 ? $"[{index}] {title}" : title;
        }
    }

    /// <summary>
    ///     Exception class used by own test architecture. DO NO USE IN Testrunner Actions!!!
    /// </summary>
    internal class MDTestTearDownException : MDTestExceptionBase
    {
        private const string Msg = "TEST TEAR DOWN FAILED!";

        public MDTestTearDownException(string message, Exception exception, int rowIndex = -1)
            : base(Msg, message, exception, rowIndex)
        {
        }

        public MDTestTearDownException(Exception exception, int rowIndex = -1)
            : base(Msg, exception, rowIndex)
        {
        }

        public MDTestTearDownException(string message, int rowIndex = -1)
            : base(Msg, message, rowIndex)
        {
        }
    }

    /// <summary>
    ///     Exception class used by own test architecture. DO NO USE IN Testrunner Actions!!!
    /// </summary>
    internal class MDTestSetupException : MDTestExceptionBase
    {
        private const string Msg = "TEST SETUP FAILED!";

        public MDTestSetupException(string message, Exception exception, int rowIndex = -1)
            : base(Msg, message, exception, rowIndex)
        {
        }

        public MDTestSetupException(Exception exception, int rowIndex = -1)
            : base(Msg, exception, rowIndex)
        {
        }

        public MDTestSetupException(string message, int rowIndex = -1)
            : base(Msg, message, rowIndex)
        {
        }
    }


    /// <summary>
    ///     Exception class used by own test architecture. DO NO USE IN Testrunner Actions!!!
    /// </summary>
    internal class MDTestExcecutionException : MDTestExceptionBase
    {
        private const string Msg = "TEST EXCECUTION FAILED!";

        public MDTestExcecutionException(string message, Exception exception, int rowIndex = -1)
            : base(Msg, message, exception, rowIndex)
        {
        }

        public MDTestExcecutionException(Exception exception, int rowIndex = -1)
            : base(Msg, exception, rowIndex)
        {
        }

        public MDTestExcecutionException(string message, int rowIndex = -1)
            : base(Msg, message, rowIndex)
        {
        }
    }
}