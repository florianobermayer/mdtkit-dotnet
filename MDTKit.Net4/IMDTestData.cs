#region

using System.Collections.Generic;

// ReSharper disable InconsistentNaming

#endregion

namespace MDTKit
{
    public interface IMDTestData
    {
        IMDTestTable MDTestTable { get; set; }
        Dictionary<string, object> PreConstants { get; set; }
        Dictionary<string, object> PostConstants { get; set; }
    }
}