#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using MDTKit.Linter;
using Newtonsoft.Json;

#endregion

namespace MDTKit
{
    public class MDTestData : IMDTestData
    {
        public const string TestDataFolderName = "testdata";
        private static string _testDataBasePath;

        private MDTestData(IMDTestTable testTable, Dictionary<string, dynamic> preConstants,
            Dictionary<string, dynamic> postConstants)
        {
            MDTestTable = testTable;
            PreConstants = preConstants;
            PostConstants = postConstants;
        }

        internal static string TestDataBasePath
        {
            set { _testDataBasePath = value; }
            private get
            {
                return _testDataBasePath ??
                       (_testDataBasePath =
                           Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\", TestDataFolderName)));
            }
        }
        

        public IMDTestTable MDTestTable { get; set; }
        public Dictionary<string, dynamic> PreConstants { get; set; }
        public Dictionary<string, dynamic> PostConstants { get; set; }


        public static IMDTestData Create(string testfilename, string testname)
        {
            if (!testfilename.EndsWith(".md"))
            {
                testfilename += ".md";
            }

            var results =
                Directory.GetFileSystemEntries(TestDataBasePath, "*.md", SearchOption.AllDirectories)
                    .Select(filepath => new FileInfo(filepath))
                    .Where(info => info.Name == testfilename)
                    .ToList();

            if (results.Count > 1)
            {
                throw new MDParserException($"Multiple files found: '{testfilename}'");
            }

            if (results.Count == 1)
            {
                var file = results.First();
                return Create(file, testname);
            }

            throw new MDParserException($"No file found: '{testfilename}'");
        }

        public static IMDTestData Create(string testname, Type testClassType = null)
        {
            // if test class represents a single test method, this allowes to simply name the testname 
            var filename = testClassType?.Name ?? testname;
            return Create(filename, testname);
        }


        public static IMDTestData Create(FileInfo file, string testname)
        {
            var parsingResult = GetParsingResult(file);
            dynamic result = JsonConvert.DeserializeObject(parsingResult);

            RaiseLinterErrors(result.linter);

            dynamic testCase = ((IEnumerable) result.tests).Cast<dynamic>().FirstOrDefault(it => it.title == testname);

            if (testCase == null)
            {
                throw new MDParserException($"Cannot find '{testname}' in file '{file.Name}'!");
            }

            return new MDTestData(ExtractTestTable(testCase), ExtractPrePostConstants(testCase, isPre: true),
                ExtractPrePostConstants(testCase, isPre: false));
        }

        

        private static string GetParsingResult(FileInfo parsedFile)
        {
            var nodePath = GetExePath("node.exe");
            if (nodePath == null) {
                throw new MDParserException("Cannot find 'node' in PATH!");
            }

            StringBuilder inputStringBuilder = new StringBuilder();
            StringBuilder errorStringBuilder = new StringBuilder();

            var exitCode = GetExitCode(nodePath, $"\"{MDTKitModulePath}\" \"{parsedFile.FullName.Trim('"', '\'')}\"",
                inputStringBuilder, errorStringBuilder, ModuleWorkingDirectory);
            

            if (exitCode != 0) {
                TryInitNodeModule();
                inputStringBuilder.Clear();
                errorStringBuilder.Clear();
                exitCode = GetExitCode(nodePath, $"\"{MDTKitModulePath}\" \"{parsedFile.FullName.Trim('"', '\'')}\"",
                inputStringBuilder, errorStringBuilder, ModuleWorkingDirectory);

                if (exitCode != 0) {
                    throw new MDParserException(
                     $"Calling node module 'mdtkit-parser' failed. ExitCode: {exitCode} ({errorStringBuilder})");
                }
                
            }
            return inputStringBuilder.ToString();
        }

        private static void TryInitNodeModule()
        {
            Console.WriteLine("Initializing node module mdtkit-parser (updating node modules)...");
            GetExitCode(GetExePath("npm.cmd"), "update", workingDirectory: ModuleWorkingDirectory);
        }

        private static string ModuleWorkingDirectory
        {

            get
            {
                var result = ConfigurationManager.AppSettings.Get("mdtkit-parser-path");
                if (string.IsNullOrWhiteSpace(result) || !Directory.Exists(result))
                {
                    throw new MDParserException("'mdtkit-parser-path' not specified properly in App.config. Please add the path to the mdtkit-parser node module to App.config." );
                }

                return Path.GetFullPath(result);

            }
        } 

        // ReSharper disable once InconsistentNaming
        public static string MDTKitModulePath => Path.Combine(ModuleWorkingDirectory, "src", "mdtkit-parser-cli.js");

        private static void RaiseLinterErrors(dynamic linter)
        {
            var exceptions = new List<MDLinterException>();
            foreach (var error in linter.errors)
            {
                exceptions.Add(new MDLinterException((string) error.message, Location.FromDynamic(error.location),
                    (string) error.path));
            }

            foreach (var stackElem in linter.stack)
            {
                exceptions.Add(new MDLinterStackException((string) stackElem.blockName,
                    Location.FromDynamic(stackElem.location), (string) stackElem.path));
            }

            if (exceptions.Count == 1)
            {
                throw exceptions.First();
            }

            if (exceptions.Any())
            {
                throw new AggregateException(exceptions);
            }
        }

        private static Dictionary<string, dynamic> ExtractPrePostConstants(dynamic testCase, bool isPre)
        {
            var result = new Dictionary<string, dynamic>();

            var constants = isPre ? testCase.preConstants : testCase.postConstants;
            foreach (var constant in constants)
            {
                result.Add((string) constant.name, CastToCorrectType(constant, isPrePost: true));
            }

            return result;
        }

        private static IMDTestTable ExtractTestTable(dynamic testCase)
        {
            var headings =
                ((IEnumerable) testCase.table.header).Cast<dynamic>().Select(it => (string) it.value.title).ToList();
            var content = new List<Dictionary<string, dynamic>>();

            foreach (var row in testCase.table.content)
            {
                var rowDict = new Dictionary<string, dynamic>();
                for (var i = 0; i < headings.Count; i++)
                {
                    rowDict[headings[i]] = CastToCorrectType(row[i]);
                }
                content.Add(rowDict);
            }

            return new MDTestTable(headings, content);
        }

        private static dynamic CastToCorrectType(dynamic entry, bool isPrePost = false)
        {
            string type = entry.validType;
            dynamic value = isPrePost ? entry.expression.value : entry.value;
            string stringValue = value?.ToString(CultureInfo.InvariantCulture);
            switch (type)
            {
                case "boolean":
                    try
                    {
                        return Convert.ToBoolean(value);
                    }
                    catch
                    {
                        return false;
                    }
                case "number":
                    try
                    {
                        return stringValue.Contains(NumberFormatInfo.InvariantInfo.CurrencyDecimalSeparator)
                            ? double.Parse(stringValue, CultureInfo.InvariantCulture)
                            : int.Parse(stringValue, CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        if (stringValue == null)
                        {
                            return 0;
                        }

                        return stringValue.Contains(NumberFormatInfo.InvariantInfo.CurrencyDecimalSeparator) ? 0.0 : 0;
                    }
                case "date":
                    try
                    {
                        return Convert.ToDateTime(value, CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        return (DateTime?)null;
                    }
                default:
                    return (string) value;
            }
        }

        private static int GetExitCode(string processName, string arguments, StringBuilder inputStringBuilder = null, StringBuilder errorStringBuilder = null, string workingDirectory  = null)
        {


            var processInfo = new ProcessStartInfo
            {
                FileName = processName,
                Arguments = arguments,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true,

            };
            if (workingDirectory != null)
            {
                processInfo.WorkingDirectory = workingDirectory;
            }

            var process = Process.Start(processInfo);

            if (inputStringBuilder != null)
            {
                while (!process.StandardOutput.EndOfStream) {
                    inputStringBuilder.AppendLine(process.StandardOutput.ReadLine());
                }
            }

            if (errorStringBuilder != null)
            {
                while (!process.StandardError.EndOfStream)
                {
                    errorStringBuilder.AppendLine(process.StandardError.ReadLine());
                }
            }

            process.WaitForExit();
            return process.ExitCode;
        }

       
        private static string GetExePath(string exeName)
        {
            var enviromentPath = Environment.GetEnvironmentVariable("PATH")?.Split(';');
            return enviromentPath?.Select(x => Path.Combine(x, exeName))
                .FirstOrDefault(File.Exists);
        }
    }
}