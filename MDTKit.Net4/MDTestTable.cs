#region

using System.Collections.Generic;

#endregion

namespace MDTKit
{
    // ReSharper disable once InconsistentNaming
    internal class MDTestTable : IMDTestTable
    {
        public MDTestTable(List<string> headings, List<Dictionary<string, dynamic>> rows)
        {
            Headings = headings;
            Rows = rows;
        }

        public List<string> Headings { get; }
        public List<Dictionary<string, dynamic>> Rows { get; }
    }
}